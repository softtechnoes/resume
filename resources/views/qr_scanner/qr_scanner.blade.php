@extends('master1')

@section('title')
QR COde Scanner
@endsection

@section('content')
    <div class="container" style="margin-top:15%">
        <h3>Generate a QR Code</h3>
        <form action='/generate-qr-scanner' method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Enter information to save in QR code" name='info' class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mt-2">
                    <input type="submit" value="Generate" class="btn btn-info">
                </div>
            </div>

        </form>
    </div>
@endsection