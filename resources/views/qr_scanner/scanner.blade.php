@extends('master1')

@section('title')
Scan QR Code
@endsection

@section('content')
<div class="container" style="margin-top:10%">
    <div class="row">
        <div class="col-md-6">
            <video id="preview"></video>
        </div>
        <div class="col-md-6">
            <span id="content"></span>
        </div>
    </div>
    
</div>

@push('js')
<script>
    document.addEventListener("DOMContentLoaded", event => {
      let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      Instascan.Camera.getCameras().then(cameras => {
        scanner.camera = cameras[cameras.length - 1];
        scanner.start();
      }).catch(e => console.error(e));
    
      scanner.addListener('scan', content => {
        console.log(content);
        $('#content').html(content);
      });
    });
    </script>
@endpush
@endsection