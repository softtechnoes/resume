@extends('master')
@section('title')
    Attendance
@endsection
@section('content')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
{{-- <div class="container" > --}}
    <div class="profile-content section" >
        <div class="container">
          <div class="row" style="margin-top:10%">
              <div class="col-md-3">
                  <div class="profile-picture">
                
                          <div class="wrapper1">
                              <div class="profile-card js-profile-card">
                                <div class="profile-card__img">
                                  {{-- <img src="https://res.cloudinary.com/muhammederdem/image/upload/v1537638518/Ba%C5%9Fl%C4%B1ks%C4%B1z-1.jpg" alt="profile card"> --}}
                                  @if(Auth::user()->image=='#')
                                      <img src="{{asset("images/user/user_male.png")}}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                                  @else
                                      <img src="{!! Auth::user()->image !!}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                                  @endif
                                </div>
                            
                                <div class="profile-card__cnt js-profile-cnt">
                                  <div class="profile-card__name">
                                      {{Auth::user()->name}}
                                   </div>
                                  <div class="profile-card__txt">{{Auth::user()->job}} </div>
                                  <div class="profile-card-loc">
                                    <i class="fa fa-map-marker"></i>
                                    <span class="profile-card-loc__txt">
                                      {{Auth::user()->city}}
                                    </span>
                                  </div>
                            
                            
                            
                                  <div class="profile-card-ctr">
                                      <a href="#">
                                          <button class="profile-card__button button--blue"><i class="fa fa-upload"></i>Upload Picture </button>
                                      </a>
                                      <a href="#">
                                          <button class="profile-card__button button--orange"><i class="fa fa-download"></i>Download </button>
                                      </a>
                                     
                                  </div>
                                </div>
                            
                              </div>
                            
                            </div>
                            
                      
                  </div>
              </div>
              
              <div class="col-md-9">
                    @if (Session::has('success'))
                        <div class="alert alert-success alert-with-icon" data-notify="container">
                            <div class="container">
                            <div class="alert-wrapper">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="nc-icon nc-simple-remove"></i>
                                </button>
                                <div class="message"><i class="nc-icon nc-bell-55"></i> {!! \Session::get('success') !!}</div>
                                </div>
                            </div>
                        </div>
                    @endif
                <div class="row">
                    <div id='calendar'></div>
                </div>
              </div>
          </div>
          
        </div>
      </div>
    {{-- </div> --}}
@stop


@push('scripts')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                @foreach($data as $dat)
                {
                    title : '{{ $dat->id }}',
                    start : '{{ $dat->created_at }}',
                    url : '{{ route('tasks.edit', $dat->id) }}'
                },
                @endforeach
            ]
        })
    });
</script>
@endpush