@extends('master1')
@section('title')
    Edit Profile
@endsection
@section('content')

{{-- <div class="container" > --}}
    <div class="profile-content section" >
      <div class="container">
        <div class="row" style="margin-top:10%">
            <div class="col-md-3">
                <div class="profile-picture">
                    
                        {{-- <div class="our-team">
                            <div class="picture">
                                @if(Auth::user()->image=='#')
                                    <img src="{{asset("images/user/user_male.png")}}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                                @else
                                    <img src="{!! Auth::user()->image !!}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                                @endif
                            </div>
                          <div class="team-content">
                            <h4 class="title text-center">{{Auth::user()->name}}
                                <br />
                                <small>{{Auth::user()->job}}</small>
                                </h4>
                          </div>
                          <ul class="social">
                            <li><a href="https://codepen.io/collection/XdWJOQ/" class="fa fa-facebook" aria-hidden="true"></a></li>
                            <li><a href="https://codepen.io/collection/XdWJOQ/" class="fa fa-twitter" aria-hidden="true"></a></li>
                            <li><a href="https://codepen.io/collection/XdWJOQ/" class="fa fa-google-plus" aria-hidden="true"></a></li>
                            <li><a href="https://codepen.io/collection/XdWJOQ/" class="fa fa-linkedin" aria-hidden="true"></a></li>
                          </ul>
                        </div> --}}
                        <div class="wrapper1">
                            <div class="profile-card js-profile-card">
                              <div class="profile-card__img">
                                @if(Auth::user()->image=='#')
                                    <img src="{{asset("images/user/user_male.png")}}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                                @else
                                    <img src="{{ asset( Auth::user()->image )}}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                                @endif
                              </div>
                          
                              <div class="profile-card__cnt js-profile-cnt">
                                <div class="profile-card__name">
                                    {{Auth::user()->name}}
                                 </div>
                                <div class="profile-card__txt">{{Auth::user()->job}} </div>
                                <div class="profile-card-loc">
                                  <i class="fa fa-map-marker"></i>
                                  <span class="profile-card-loc__txt">
                                    {{Auth::user()->city}}
                                  </span>
                                </div>
                          
                                <div class="profile-card-ctr">
                                    <a href="#">
                                        <button class="profile-card__button button--blue"><i class="fa fa-upload"></i>Upload Picture </button>
                                    </a>
                                    <a href="#">
                                        <button class="profile-card__button button--orange"><i class="fa fa-download"></i>Download </button>
                                    </a>
                                   
                                </div>
                              </div>
                          
                            </div>
                          
                          </div>
                          
                    
                </div>
            </div>
            
            <div class="col-md-9">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-with-icon" data-notify="container">
                        <div class="container">
                        <div class="alert-wrapper">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <div class="message"><i class="nc-icon nc-bell-55"></i> {!! \Session::get('success') !!}</div>
                            </div>
                        </div>
                    </div>
                @endif
                <form action="update-profile">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="col-md-6">
                            <label for="">Father name</label>
                            <input type="text" class="form-control" name="father_name" value="{{ Auth::user()->father_name }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-envelope"></i> Email</label>
                            <input type="text" class="form-control" disabled value="{{ Auth::user()->email }}">
                        </div>
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-mobile"></i> Mobile</label>
                            <input type="text" class="form-control" name="mobile" value="{{ Auth::user()->mobile }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Social links</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-facebook"></i> Facebook</label>
                            <input type="text" class="form-control" name="facebook_link" value="{{ Auth::user()->facebook_url }}">
                        </div>
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-instagram"></i> Instagram</label>
                            <input type="text" class="form-control" name="instagram_link" value="{{ Auth::user()->instagram_url }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-linkedin"></i> Linkedin</label>
                            <input type="text" class="form-control" name="linkedin_url" value="{{ Auth::user()->linkedin_url }}">
                        </div>
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-twitter"></i> Twitter</label>
                            <input type="text" class="form-control" name="twitter_url" value="{{ Auth::user()->twitter_url }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><i class="fa fa-globe"></i> Website</label>
                            <input type="text" class="form-control" name="website" value="{{ Auth::user()->website }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-user"></i> Age</label>
                            <input type="text" class="form-control" name="age" value="{{ Auth::user()->age }}">
                        </div>
                        <div class="col-md-6">
                            <label for=""><i class="fa fa-briefcase"></i> Current Job Role</label>
                            <input type="text" class="form-control" name="job" value="{{ Auth::user()->job }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><i class="fa fa-user"></i> About</label>
                            <textarea name="about" id="" class="form-control" namne="about">{{ Auth::user()->about }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><i class="fa fa-address-card"></i> Permanent Address</label>
                            <textarea name="about" class="form-control" namne="about" id="permanent_address">{{ Auth::user()->permanent_address }}</textarea>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check">
                                <label class="form-check-label">
                                <input class="form-check-input text-success" type="checkbox" value="" onclick="copyAddress()"> Current address same as Permanent address
                                <span class="form-check-sign"></span>
                                </label>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for=""><i class="fa fa-address-card"></i> Current Address</label>
                            <textarea name="about" class="form-control" namne="about" id="current_address">{{ Auth::user()->current_address }}</textarea>
                        </div>
                    </div>
                    <br><div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center"><button type="submit" class="btn btn-success">Save</button></h3>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
      </div>
    </div>
  {{-- </div> --}}

@endsection
@push('scripts')
    <script>
    // var a=["http://www.sample.com","https://www.sample.com/","https://www.sample.com#","http://www.sample.com/xyz","http://www.sample.com/#xyz","www.sample.com","www.sample.com/xyz/#/xyz","sample.com","sample.com?name=foo","http://www.sample.com#xyz","http://www.sample.c"];
    // var re=/^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
    // a.map(x=>console.log(x+" => "+re.test(x)));
    </script>
    <script>
    function copyAddress(){
        var permanent_address= $.trim($("#permanent_address").val());
        console.log(permanent_address);
        $("#current_address").val(permanent_address);
    }
    </script>
@endpush
