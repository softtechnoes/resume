<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>{{$user->name}} Resume Output</TITLE>
<META name="generator" content="BCL easyConverter SDK 5.0.210">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {
  position:relative; 
  overflow: hidden;
  /* margin: 56px 0px 92px 60px; */
  margin-top: 40px;
  padding: 0px;
  border: none;
  width: 733px;
  }
#page_1 #id1_1 {
  float:left;
  border:none;
  margin: 0px 0px 0px 33px;
  padding: 0px;
  border:none;
  width: 529px;
  overflow: hidden;
  }
#page_1 #id1_2 {
  float:left;
  border:none;
  margin: 2px 0px 0px 0px;
  padding: 0px;
  border:none;
  width: 171px;
  overflow: hidden;
  }

#page_1 #p1dimg1 {position:absolute;top:4px;left:0px;z-index:-1;width:543px;height:968px;}
#page_1 #p1dimg1 #p1img1 {width:543px;height:968px;}

#page_1 #p1inl_img1 {position:relative;width:9px;height:11px;}
#page_1 #p1inl_img2 {position:relative;width:15px;height:15px;}



#page_2 {position:relative; overflow: hidden;margin: 56px 0px 862px 62px;padding: 0px;border: none;width: 731px;height: 205px;}

#page_2 #p2dimg1 {position:absolute;top:10px;left:0px;z-index:-1;width:57px;height:195px;}
#page_2 #p2dimg1 #p2img1 {width:57px;height:195px;}




.ft0{font: bold 26px 'helvetica';color: #212121;line-height: 30px;}
.ft1{font: bold 13px 'helvetica';color: #2196f3;line-height: 16px;}
.ft2{font: bold 11px 'helvetica';color: #9e9e9e;line-height: 14px;}
.ft3{font: bold 22px 'helvetica';color: #212121;line-height: 26px;}
.ft4{font: bold 12px 'helvetica';color: #424242;line-height: 19px;}
.ft5{font: bold 15px 'helvetica';color: #212121;line-height: 18px;}
.ft6{font: bold 12px 'helvetica';color: #424242;line-height: 18px;}
.ft7{font: bold 12px 'helvetica';color: #424242;line-height: 17px;}
.ft8{font: bold 12px 'helvetica';color: #424242;line-height: 15px;}
.ft9{font: bold 13px 'helvetica';color: #212121;line-height: 16px;}
.ft10{font: bold 11px 'helvetica';color: #212121;line-height: 17px;}
.ft11{font: bold 10px 'helvetica';color: #2196f3;line-height: 19px;}
.ft12{font: bold 10px 'helvetica';color: #212121;line-height: 19px;}
.ft13{font: bold 11px 'helvetica';color: #212121;line-height: 19px;}
.ft14{font: bold 11px 'helvetica';color: #212121;line-height: 14px;}
.ft15{font: bold 11px 'helvetica';color: #212121;line-height: 16px;}
.ft16{font: bold 10px 'helvetica';color: #212121;line-height: 18px;}

.p0{text-align: left;padding-left: 120px;margin-top: 0px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 120px;margin-top: 11px;margin-bottom: 0px;}
.p2{text-align: left;padding-left: 120px;margin-top: 15px;margin-bottom: 0px;}
.p3{text-align: left;margin-top: 27px;margin-bottom: 0px;}
.p4{text-align: left;padding-left: 1px;padding-right: 47px;margin-top: 17px;margin-bottom: 0px;}
.p5{text-align: left;margin-top: 12px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 1px;margin-top: 14px;margin-bottom: 0px;}
.p7{text-align: left;padding-left: 1px;margin-top: 4px;margin-bottom: 0px;}
.p8{text-align: left;padding-left: 1px;padding-right: 71px;margin-top: 12px;margin-bottom: 0px;}
.p9{text-align: left;padding-left: 34px;padding-right: 54px;margin-top: 0px;margin-bottom: 0px;}
.p10{text-align: left;padding-left: 34px;padding-right: 67px;margin-top: 0px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 34px;padding-right: 78px;margin-top: 1px;margin-bottom: 0px;}
.p12{text-align: left;padding-left: 1px;margin-top: 12px;margin-bottom: 0px;}
.p13{text-align: justify;padding-left: 1px;padding-right: 69px;margin-top: 11px;margin-bottom: 0px;}
.p14{text-align: left;padding-left: 34px;padding-right: 52px;margin-top: 0px;margin-bottom: 0px;}
.p15{text-align: left;padding-left: 34px;padding-right: 68px;margin-top: 0px;margin-bottom: 0px;}
.p16{text-align: left;padding-left: 1px;margin-top: 15px;margin-bottom: 0px;}
.p17{text-align: left;padding-left: 1px;margin-top: 5px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 1px;padding-right: 61px;margin-top: 11px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 34px;margin-top: 1px;margin-bottom: 0px;}
.p20{text-align: left;padding-left: 34px;padding-right: 67px;margin-top: 2px;margin-bottom: 0px;}
.p21{text-align: left;padding-left: 34px;margin-top: 0px;margin-bottom: 0px;}
.p22{text-align: left;padding-left: 34px;padding-right: 52px;margin-top: 2px;margin-bottom: 0px;}
.p23{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p24{text-align: left;padding-right: 66px;margin-top: 15px;margin-bottom: 0px;}
.p25{text-align: left;padding-right: 77px;margin-top: 0px;margin-bottom: 0px;}
.p26{text-align: left;margin-top: 15px;margin-bottom: 0px;}
.p27{text-align: left;padding-right: 114px;margin-top: 6px;margin-bottom: 0px;}
.p28{text-align: left;margin-top: 7px;margin-bottom: 0px;}
.p29{text-align: left;margin-top: 22px;margin-bottom: 0px;}
.p30{text-align: left;margin-top: 6px;margin-bottom: 0px;}
.p31{text-align: left;padding-left: 1px;margin-top: 22px;margin-bottom: 0px;}
.p32{text-align: left;padding-right: 71px;margin-top: 14px;margin-bottom: 0px;}
.p33{text-align: left;padding-right: 82px;margin-top: 0px;margin-bottom: 0px;}
.p34{text-align: left;padding-right: 68px;margin-top: 0px;margin-bottom: 0px;}
.p35{text-align: left;padding-right: 45px;margin-top: 4px;margin-bottom: 0px;}
.p36{text-align: left;padding-left: 31px;margin-top: 0px;margin-bottom: 0px;}
.p37{text-align: left;padding-left: 32px;margin-top: 15px;margin-bottom: 0px;}
.p38{text-align: left;padding-left: 32px;margin-top: 4px;margin-bottom: 0px;}
.p39{text-align: left;padding-left: 65px;margin-top: 12px;margin-bottom: 0px;}
.p40{text-align: left;padding-left: 31px;margin-top: 20px;margin-bottom: 0px;}
.p41{text-align: left;padding-left: 32px;margin-top: 14px;margin-bottom: 0px;}


/* body {	
  margin: 0;
  padding: 0;
  background: rgb(230,230,230);
  
  color: rgb(50,50,50);
  font-family: 'Open Sans', sans-serif;
  font-size: 112.5%;
  line-height: 1.6em;
} */

/* ================ The Timeline ================ */

.timeline {
  position: relative;
  width: 660px;
  margin: 0 auto;
  margin-top: 20px;
  padding: 1em 0;
  list-style-type: none;
  margin-left: -319px;
}

.timeline:before {
  position: absolute;
  left: 50%;
  top: 0;
  content: ' ';
  display: block;
  width: 6px;
  height: 100%;
  margin-left: -3px;
  background: rgb(80,80,80);
  background: -moz-linear-gradient(top, rgba(80,80,80,0) 0%, rgb(80,80,80) 8%, rgb(80,80,80) 92%, rgba(80,80,80,0) 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(30,87,153,1)), color-stop(100%,rgba(125,185,232,1)));
  background: -webkit-linear-gradient(top, rgba(80,80,80,0) 0%, rgb(80,80,80) 8%, rgb(80,80,80) 92%, rgba(80,80,80,0) 100%);
  background: -o-linear-gradient(top, rgba(80,80,80,0) 0%, rgb(80,80,80) 8%, rgb(80,80,80) 92%, rgba(80,80,80,0) 100%);
  background: -ms-linear-gradient(top, rgba(80,80,80,0) 0%, rgb(80,80,80) 8%, rgb(80,80,80) 92%, rgba(80,80,80,0) 100%);
  background: linear-gradient(to bottom, rgba(80,80,80,0) 0%, rgb(80,80,80) 8%, rgb(80,80,80) 92%, rgba(80,80,80,0) 100%);
  
  z-index: 5;
}

.timeline li {
  padding: 1em 0;
}

.timeline li:after {
  content: "";
  display: block;
  height: 0;
  clear: both;
  visibility: hidden;
}

.direction-l {
  position: relative;
  width: 300px;
  float: left;
  text-align: right;
}

.direction-r {
  position: relative;
  width: 300px;
  float: right;
}

.flag-wrapper {
  position: relative;
  display: inline-block;
  
  text-align: center;
}

.flag {
  position: relative;
  display: inline;
  background: rgb(248,248,248);
  padding: 6px 10px;
  border-radius: 5px;
  
  font-weight: 600;
  text-align: left;
}

.direction-l .flag {
  -webkit-box-shadow: -1px 1px 1px rgba(0,0,0,0.15), 0 0 1px rgba(0,0,0,0.15);
  -moz-box-shadow: -1px 1px 1px rgba(0,0,0,0.15), 0 0 1px rgba(0,0,0,0.15);
  box-shadow: -1px 1px 1px rgba(0,0,0,0.15), 0 0 1px rgba(0,0,0,0.15);
}

.direction-r .flag {
  -webkit-box-shadow: 1px 1px 1px rgba(0,0,0,0.15), 0 0 1px rgba(0,0,0,0.15);
  -moz-box-shadow: 1px 1px 1px rgba(0,0,0,0.15), 0 0 1px rgba(0,0,0,0.15);
  box-shadow: 1px 1px 1px rgba(0,0,0,0.15), 0 0 1px rgba(0,0,0,0.15);
}

.direction-l .flag:before,
.direction-r .flag:before {
  position: absolute;
  top: 50%;
  right: -40px;
  content: ' ';
  display: block;
  width: 12px;
  height: 12px;
  margin-top: -10px;
  background: #fff;
  border-radius: 10px;
  border: 4px solid rgb(255,80,80);
  z-index: 10;
}

.direction-r .flag:before {
  left: -40px;
}

.direction-l .flag:after {
  content: "";
  position: absolute;
  left: 100%;
  top: 50%;
  height: 0;
  width: 0;
  margin-top: -8px;
  border: solid transparent;
  border-left-color: rgb(248,248,248);
  border-width: 8px;
  pointer-events: none;
}

.direction-r .flag:after {
  content: "";
  position: absolute;
  right: 100%;
  top: 50%;
  height: 0;
  width: 0;
  margin-top: -8px;
  border: solid transparent;
  border-right-color: rgb(248,248,248);
  border-width: 8px;
  pointer-events: none;
}

.time-wrapper {
  display: inline;
  
  line-height: 1em;
  font-size: 0.66666em;
  color: rgb(250,80,80);
  vertical-align: middle;
}

.direction-l .time-wrapper {
  float: left;
}

.direction-r .time-wrapper {
  float: right;
}

.time {
  display: inline-block;
  padding: 4px 6px;
  background: rgb(248,248,248);
}

.desc {
  margin: 1em 0.75em 0 0;
  
  font-size: 0.77777em;
  font-style: italic;
  line-height: 1.5em;
}

.direction-r .desc {
  margin: 1em 0 0 0.75em;
}

/* ================ Timeline Media Queries ================ */

@media screen and (max-width: 660px) {

.timeline {
 	width: 100%;
	padding: 4em 0 1em 0;
}

.timeline li {
	padding: 2em 0;
} 

.direction-l,
.direction-r {
	float: none;
	width: 100%;

	text-align: center;
}

.flag-wrapper {
	text-align: center;
}

.flag {
	background: rgb(255,255,255);
	z-index: 15;
}

.direction-l .flag:before,
.direction-r .flag:before {
    position: absolute;
    top: -30px;
    left: 50%;
    content: ' ';
    display: block;
    width: 12px;
    height: 12px;
    margin-left: -9px;
    background: #fff;
    border-radius: 10px;
    border: 4px solid rgb(255,80,80);
    z-index: 10;
}

.direction-l .flag:after,
.direction-r .flag:after {
	content: "";
	position: absolute;
	left: 50%;
	top: -8px;
	height: 0;
	width: 0;
	margin-left: -8px;
	border: solid transparent;
	border-bottom-color: rgb(255,255,255);
	border-width: 8px;
	pointer-events: none;
}

.time-wrapper {
	display: block;
	position: relative;
	margin: 4px 0 0 0;
	z-index: 14;
}

.direction-l .time-wrapper {
	float: none;
}

.direction-r .time-wrapper {
	float: none;
}

.desc {
	position: relative;
	margin: 1em 0 0 0;
	padding: 1em;
	background: rgb(245,245,245);
	-webkit-box-shadow: 0 0 1px rgba(0,0,0,0.20);
	-moz-box-shadow: 0 0 1px rgba(0,0,0,0.20);
	box-shadow: 0 0 1px rgba(0,0,0,0.20);
  z-index: 15;
}

.direction-l .desc,
.direction-r .desc {
	position: relative;
	margin: 1em 1em 0 1em;
	padding: 1em;
	
  z-index: 15;
}

}

@media screen and (min-width: 400px ?? max-width: 660px) {

.direction-l .desc,
.direction-r .desc {
	margin: 1em 4em 0 4em;
}

}
.round_img{
    border-radius: 50%;
    height:100px;
    margin-left:26px; 
}
.location{
  margin: 10px;
  color: red;
}
.company{
  margin:10px
}
@media print {
    #Header, #Footer { display: none !important; }
}

</STYLE>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
</HEAD>

<BODY>
<DIV id="page_1">
<DIV id="p1dimg1">
    <IMG src="{{asset($user->image)}}" class="round_img">
</DIV>
<DIV>
<DIV id="id1_1">
<P class="p0 ft0">{{$user->name}} </P>
<P class="p1 ft1">{{$user->job}}</P>
<P class="p2 ft2"><IMG src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAALCAYAAACtWacbAAAARUlEQVQYlY2QwQoAMAhCc/T/v+xOgbVRdTNUXoGkTXNGh5m5CgCpliRSUzXozlVEsoYQ4L+mCK7AW9MDrjxtkxrTEZuPX1ptJA1YS5FFAAAAAElFTkSuQmCC" id="p1inl_img1"> {{$user->cureent_address}}</P>
<P class="p3 ft3">Proﬁle</P>
<P class="p4 ft4">{{$user->about}} </P>
{{-- <p class="p4 ft4">Example: Energetic Web Designer with 3 years experience creating and maintaining functional, attractive, and responsive websites for travel companies. Clear understanding of modern technologies and best design practices. Experienced with WordPress and Drupal. Proven track record of raising UX scores and customer retention.</p> --}}



<!-- The Timeline -->

<ul class="timeline">

  @foreach ($user->experience as $experience)
      {{-- {{$experience}}<br> --}}



      <li>
        <div class="direction-r">
          <div class="flag-wrapper">
            <span class="flag">{{$experience->role}}</span>
            <span class="time-wrapper"><span class="time">{{$experience->from}} - {{$experience->to}}</span></span>
          </div>
          <div class="company">
            <i class="fa fa-briefcase"></i> {{$experience->company}}
          </div>
          <div class="location">
            <i class="fa fa-map-marker"></i> {{$experience->city}}
          </div>
          
          <div class="desc">{{$experience->description}}</div>
        </div>
      </li>
  @endforeach
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Freelancer</span>
				<span class="time-wrapper"><span class="time">2013 - present</span></span>
			</div>
			<div class="desc">My current employment. Way better than the position before!</div>
		</div>
	</li>
  
</ul>


<P class="p5 ft3">Employment History</P>
<P class="p6 ft5">Web Designer at Expedia Group, New York</P>
<P class="p7 ft1">January 2017 – May 2018</P>
<P class="p8 ft4">Expedia Group is a global travel company with websites which are primarily travel fare aggregators. As the Web Designer, my core activities included:</P>
<P class="p9 ft6">Planning site designs, functionality and navigation, along with audience funnels and data capture points.</P>
<P class="p10 ft7">Building wireframes & prototypes which were then turned into functional and responsive digital products.</P>
<P class="p11 ft4">Reviewing UX with multiple teams and making necessary edits to accommodate technical or business concerns. Raised UX scores by 38%. Handling all composition, color, illustration, typography, and branding for projects.</P>
<P class="p12 ft5">Web Designer at FarePortal, New York</P>
<P class="p7 ft1">February 2016 – December 2016</P>
<P class="p13 ft4">FarePortal is a travel technology company where the ﬂagship product CheapOair receives over 100 million visitors annually. As the Web Designer, my core activities included:</P>
<P class="p14 ft6">Designing, building, and maintaining marketing email creative using <NOBR>data-driven</NOBR> responsive templates.</P>
<P class="p15 ft6">Providing expertise on industry standards, best practices, and proper coding techniques to achieve correct rendering in all email environments. Performing quality assurance and troubleshooting code rendering across multiple desktop and mobile devices. Improved customer retention by 17%. Creating landing pages using WordPress CMS.</P>
<P class="p16 ft5">Web Designer at The Points Guy, New York</P>
<P class="p17 ft1">March 2015 – November 2015</P>
<P class="p18 ft4">The Points Guy is a site devoted to helping over 5 million monthly readers stay up to date on travel news. As the Web Designer, my core activities included:</P>
<P class="p19 ft8">Creating homepage assets for both desktop & mobile experiences.</P>
<P class="p20 ft6">Developing site content and graphics in partnership with writers and creative director. Spearheaded 4 projects simultaneously.</P>
<P class="p21 ft8">Designing images, audio enhancements, icons, and banners.</P>
<P class="p22 ft4">Presenting concepts and ideas consistent with company branding guidelines to the creative team.</P>
</DIV>
<DIV id="id1_2">
<P class="p23 ft9">Details</P>
<P class="p24 ft10">{{$user->cureent_address}}</P>
<P class="p25 ft12"><NOBR>{{$user->mobile}}</NOBR> <SPAN class="ft11">{{$user->email}}</SPAN></P>
<P class="p26 ft2">DATE / PLACE OF BIRTH</P>
<P class="p27 ft13">{{$user->dob}} {{$user->city}}</P>
<P class="p26 ft2">NATIONALITY</P>
<P class="p28 ft14">{{$user->citizenship}}</P>
{{-- <P class="p29 ft2">DRIVING LICENSE</P>
<P class="p30 ft14">Full</P> --}}
<P class="p31 ft9"><IMG src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAUElEQVQokbWTSwpAMQgDTXn3v/J0V4Rnaz84O8GYiCjAbmnXSjP7fCEpjQFo9ANbomjAMrZ3iXja+SfO3OqcIwBFieqdZ9TdOaP5Safo5as6tacnBwrXELUAAAAASUVORK5CYII=" id="p1inl_img2"> Skills</P>
  <ul type="none">
  @foreach ($user->skills as $skill)
      {{-- {{$skill}}<br> --}}
      {{-- <span class="p32 ft10"><i class="fa fa-gears"></i> {{$skill->skill}}</span> --}}
      <li class="ft10" style="margin-left:-22px;"><i class="fa fa-gears"></i> {{$skill->skill}}</li>
  @endforeach
</ul>
{{-- <P class="p32 ft10">WordPress, Drupal, Joomla</P>
<P class="p33 ft10">HTML5, CSS, JS, jQuery</P>
<P class="p34 ft15">Adobe Photoshop & Illustrator</P>
<P class="p23 ft14">Sketch</P>
<P class="p35 ft16">Time management <NOBR>Deadline-driven</NOBR> Eﬀective communicator Team player Energetic and inventive</P> --}}
</DIV>
</DIV>
</DIV>
<DIV id="page_2">
<DIV id="p2dimg1">
<IMG src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADCADkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDf+K3xR8S+Ed1ppnhye0jk+RdWu1WSMk7wPLCEru+UON5zgHMeKw/hr8aPEeqXtvp2u6VJqVu9xDbvqlrFsNu0rlUMoA2YLFVGNnAP3jXeePfBninXtRGo+HvFP2HbafZjplzGXtLjJYO0incpyj4wY2+6PqKfgj4feI9Om8zxXrVpdWkSKltpWnReTaxskyTpLtQRrvDqc/JyDyxHFAHplFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAEN3I0VlPIhwyxswPoQKmqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKwfEXh2bXLnS7i31e70+SwuPOBgwQ4IwQQeM4yATkYZgQQa3qAGyRrLG0bjKsCpHqDTqKKACiiigAooooAKKKKACisfWfFGk6Bv/ALQmnHlxGeXyLSWfyYxn55PLVtina2C2AdrY+6cU/DXj7wv4vuJ7fQtWjup4EDyRmN422k4yA6gkZwCRnGRnqKAOkooooAKKKKACiiigDw/4v+Ov+EU8Q6xpP9nfav7c8PxW3m+fs8j57pd2Np3f6zOMjp71yH7OP/JQ9Q/7BUn/AKNir2Pxr8JtB8eazDqmqXepQzxW626rayIqlQzNk7kY5y57+lHgr4TaD4D1mbVNLu9Smnlt2t2W6kRlCllbI2opzlB39aAO8ooooAKKKKACiiigAopGZUUsxCqBkknAApaACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAEZVdSrAMpGCCMgiloooAKKKKACiiigAooooA/9k=" id="p2img1"></DIV>


<P class="p36 ft3">Education</P>
<P class="p37 ft5">Bachelor's Degree in Interaction Design, Sterling College, New York</P>
<P class="p38 ft1">2014</P>
<P class="p39 ft8">Excelled in UI/UX coursework.</P>
<P class="p40 ft3">Courses</P>
<P class="p41 ft5">Advanced User Interface Design, Udemy</P>
<P class="p38 ft1">May 2016</P>
</DIV>
</BODY>
</HTML>
