@extends('master1')

@section('title')
Homepage : : Welcome
@endsection

@section('hero')
<div class="page-header section-dark" style="background-image: url({{ asset('./assets/img/hero.jpg')}}">
  <div class="filter"></div>
  <div class="content-center">
    <div class="container">
      <div class="title-brand">
        <h5 class="presentation-title" style="font-size: 7em;">AutoCv</h5>
        {{-- <div class="fog-low">
          <img src="{{ asset('assets/img/fog-low.png')}}" alt="">
        </div>
        <div class="fog-low right">
          <img src="{{ asset('assets/img/fog-low.png')}}" alt="">
        </div> --}}
      </div>
      <h2 class="presentation-subtitle text-center">Make your professional Reusme with stylish templates </h2>
    </div>
  </div>
  {{-- <div class="moving-clouds" style="background-image: url({{ asset('./assets/img/clouds.png')}}); "></div> --}}
  {{-- <h6 class="category category-absolute">Designed and coded by 
    <a href="https://www.creative-tim.com" target="_blank">
      <img src="{{ asset('assets/img/creative-tim-white-slim2.png')}}" class="creative-tim-logo">
    </a>
  </h6> --}}
</div>
@endsection

@section('content')
<div class="section text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto">
        <h2 class="title">Let's talk product</h2>
        <h5 class="description">This is the paragraph where you can write more details about your product. Keep you user engaged by providing meaningful information. Remember that by this time, the user is curious, otherwise he wouldn't scroll to get here. Add a button if you want the user to see more.</h5>
        <br>
        <a href="#paper-kit" class="btn btn-danger btn-round">See Details</a>
      </div>
    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-3">
        <div class="info">
          <div class="icon icon-danger">
            <i class="nc-icon nc-album-2"></i>
          </div>
          <div class="description">
            <h4 class="info-title">Beautiful Gallery</h4>
            <p class="description">Spend your time generating new ideas. You don't have to think of implementing.</p>
            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="info">
          <div class="icon icon-danger">
            <i class="nc-icon nc-bulb-63"></i>
          </div>
          <div class="description">
            <h4 class="info-title">New Ideas</h4>
            <p>Larger, yet dramatically thinner. More powerful, but remarkably power efficient.</p>
            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="info">
          <div class="icon icon-danger">
            <i class="nc-icon nc-chart-bar-32"></i>
          </div>
          <div class="description">
            <h4 class="info-title">Statistics</h4>
            <p>Choose from a veriety of many colors resembling sugar paper pastels.</p>
            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="info">
          <div class="icon icon-danger">
            <i class="nc-icon nc-sun-fog-29"></i>
          </div>
          <div class="description">
            <h4 class="info-title">Delightful design</h4>
            <p>Find unique and handmade delightful designs related items directly from our sellers.</p>
            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section section-dark text-center">
  <div class="container">
    <h2 class="title">Let's talk about us</h2>
    <div class="row"><div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="card card-profile card-plain">
          <div class="card-avatar">
            <a href="#avatar">
              <img src="{{ asset('soft_technoes.jpg')}}" alt="...">
            </a>
          </div>
          <div class="card-body">
            <a href="#paper-kit">
              <div class="author">
                <h4 class="card-title">Soft Technoes</h4>
                <h6 class="card-category">Lead Developer</h6>
              </div>
            </a>
            <p class="card-description text-center">
              Teamwork is so important that it is virtually impossible for you to reach the heights of your capabilities or make the money that you want without becoming very good at it.
            </p>
          </div>
          <div class="card-footer text-center">
            <a href="https://twitter.com/Softtechnoes" class="btn btn-link btn-just-icon btn-neutral" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://stackoverflow.com/users/11720498/soft-technoes" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-stack-overflow"></i></a>
            <a href="https://www.linkedin.com/in/soft-technoes-252478193/?originalSubdomain=in" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
          </div>
        </div>
      </div>
      {{-- <div class="col-md-4">
        <div class="card card-profile card-plain">
          <div class="card-avatar">
            <a href="#avatar">
              <img src="{{ asset('assets/img/faces/joe-gardner-2.jpg')}}" alt="...">
            </a>
          </div>
          <div class="card-body">
            <a href="#paper-kit">
              <div class="author">
                <h4 class="card-title">Sophie West</h4>
                <h6 class="card-category">Designer</h6>
              </div>
            </a>
            <p class="card-description text-center">
              A group becomes a team when each member is sure enough of himself and his contribution to praise the skill of the others. No one can whistle a symphony. It takes an orchestra to play it.
            </p>
          </div>
          <div class="card-footer text-center">
            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card card-profile card-plain">
          <div class="card-avatar">
            <a href="#avatar">
              <img src="{{ asset('assets/img/faces/erik-lucatero-2.jpg')}}" alt="...">
            </a>
          </div>
          <div class="card-body">
            <a href="#paper-kit">
              <div class="author">
                <h4 class="card-title">Robert Orben</h4>
                <h6 class="card-category">Developer</h6>
              </div>
            </a>
            <p class="card-description text-center">
              The strength of the team is each individual member. The strength of each member is the team. If you can laugh together, you can work together, silence isn’t golden, it’s deadly.
            </p>
          </div>
          <div class="card-footer text-center">
            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
          </div>
        </div>
      </div> --}}
    </div>
  </div>
</div>
<div class="section landing-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto">
        <h2 class="text-center">Keep in touch?</h2>
        <form class="contact-form" action="send-query" method="post">
          @csrf
          <div class="row">
            <div class="col-md-6">
              <label>Name</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="nc-icon nc-single-02"></i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="Name">
              </div>
            </div>
            <div class="col-md-6">
              <label>Email</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="nc-icon nc-email-85"></i>
                  </span>
                </div>
                <input type="text" name="email" class="form-control" placeholder="Email">
              </div>
            </div>
          </div>
          <label>Message</label>
          <textarea class="form-control" rows="4" name="message" placeholder="Tell us your thoughts and feelings..."></textarea>
          <div class="row">
            <div class="col-md-4 ml-auto mr-auto">
              <button type="submit" class="btn btn-danger btn-round">Send Message</button>
            </div>
          </div>
        </form><br>
        
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
      </div>
    </div>
    
  </div>
</div>

@endsection
