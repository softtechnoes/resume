@push('js')
{{-- <script src="{{asset('js/notify.min.js')}}"></script> --}}
<script>
    function editNote(value){
        
        var note           = $(value).text();
        var id             = $(value).attr('data-value');
        var note_row       = $(value).attr('note-row');
        var note_cell_id   = $(value).attr('id');
        var address        = $(value).attr('address');
        $('#'+note_cell_id).replaceWith(function(){
            return $('<td><input type="text" class="form-control" id="'+note_cell_id+'" value="'+note+'" data-id="'+id+'" note-row="'+note_row+'" onchange="updateNote(this)" address='+address+'></td>');
        });
        
        $('#editable-address-'+id).replaceWith(function(){
            return $('<td><input type="text" class="form-control" id="editable-address-'+id+'" value="'+address+'" data-id="'+id+'" note-row="'+note_row+'" onchange="updateNoteAddress(this)"></td>');
        });
    }
    function editNoteBtn(value){
        var note            = $(value).val();
        var id              = $(value).attr('data-value');
        var note_row        = $(value).attr('note-row');
        var note_cell_id    = $(value).attr('cell-id');
        var address         = $(value).attr('address');

        $('#'+note_cell_id).replaceWith(function(){
            return $('<td><input type="text" class="form-control" id="'+note_cell_id+'" value="'+note+'" data-id="'+id+'" note-row="'+note_row+'" onchange="updateNote(this)"></td>');
        });
        $('#editable-address-'+id).replaceWith(function(){
            return $('<td><input type="text" class="form-control" id="editable-address-'+id+'" value="'+address+'" data-id="'+id+'" note-row="'+note_row+'" onchange="updateNoteAddress(this)"></td>');
        });
    }

    function updateNote(value){
        var note_value       = $(value).val();
        var id               = $(value).attr('id');
        var note_row         = $(value).attr('note-row');
        var data_id          = $(value).attr('data-id');
        var address          = $(value).attr('address');
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
            url: "{{ url('update-note') }}"+'/'+data_id,
            data: { 'id': data_id, 'note':note_value, 'data_id':id, 'address':address },
            type: 'get',
            dataType: 'json',
            success: function(res){
                // console.log(res);
                // $.notify('{!!Lang::get('site_lang.successful_update')!!}', {
                //     className: 'success'
                // });
                $.growl.notice({ message: "Done !" });
                $('#'+res[2]).replaceWith(function(){
                    return $('<td ondblclick="editNote(this)" data-value="'+res[0]+'" id="'+res[2]+'" address='+address+' note-row="note-'+res[0]+'">'+note_value+'</td>');
                });
                $('#editable-address-'+res[0]).replaceWith(function(){
                    return $('<td data-value="'+res[0]+'" id="editable-address-'+res[0]+'" note-row="note-'+res[0]+'">'+address+'</td>');
                });
            }
        });
    }
    function updateNoteAddress(value){
        var id               = $(value).attr('id');
        var note_row         = $(value).attr('note-row');
        var data_id          = $(value).attr('data-id');
        var address          = $(value).val(); 
      
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
            url: "{{ url('update-note') }}"+'/'+data_id,
            data: { 'id': data_id, 'data_id':id, 'address':address },
            type: 'get',
            dataType: 'json',
            success: function(res){
                // $.notify('{!!Lang::get('site_lang.successful_update')!!}', {
                //     className: 'success'
                // });
                $.growl.notice({ message: "Done !" });
                var id         = res[0][2];
                var address    = res[0][1];
                var note_row   = res[0][0];
                var note_value = res[1][0].note;
            
                $('#editable-note-'+note_row).replaceWith(function(){
                    return $('<td ondblclick="editNote(this)" data-value="'+note_row+'" id="editable-note-'+note_row+'" note-row="note-'+note_row+'" address="'+address+'">'+note_value+'</td>');
                });
              
                $('#'+id).replaceWith(function(){
                    return $('<td data-value="'+id+'" id="editable-address-'+note_row+'" note-row="note-'+note_row+'">'+address+'</td>');
                });
            }
        });
    }

     function markActive(value){
         var check_id=$(value).attr('id');
         var data_id=$(value).val();
         
        //  console.log(data_id);
        //  var test=$('#'+check_id).hasAttribute("checked");
         var note_status = document.getElementById(check_id).hasAttribute("checked");
         if(note_status==false){
             
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
            url: "{{ url('change-status') }}"+'/'+data_id,
            data: { 'id': data_id },
            type: 'get',
            dataType: 'json',
            success: function(res){
                // $.notify('{!!Lang::get('site_lang.note_marked_completed')!!}', {
                //     className:'success'
                // });
                $.growl.notice({ message: "Done !" });
                $("#check-"+res).attr('checked','true');
                $("#editable-note-"+res).attr('style','text-decoration: line-through;color:red');
                // $("#show-note-"+res).hide();
            }
        });
         }
         else{
            $("#"+check_id).removeAttr('checked');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });
            $.ajax({
                url: "{{ url('change-status-undone') }}"+'/'+data_id,
                data: { 'id': data_id },
                type: 'get',
                dataType: 'json',
                success: function(res){
                    console.log(res);
                    // $.notify('{!!Lang::get('site_lang.note_marked_active')!!}', {
                    //     className:'success'
                    // });
                    $.growl.notice({ message: "Done !" });
                    $("#check-"+res).removeAttr('checked');
                    $("#editable-note-"+res).removeAttr('style');
                    // $('del').hide();
                    // $("#show-note-"+res).show();
                }
            });
         }
        
     }
</script>
@endpush