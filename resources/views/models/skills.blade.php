@push('css')
  <style>
    .modal { overflow-y: auto }
  </style>
@endpush 
 <!-- Edit Master -->

 @if(count($personal_skills)!=0)
 @foreach ($personal_skills as $personal_skill)
   <div class="modal fade" id="editPersonalSkills_{{$personal_skill->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title text-center" id="exampleModalLabel">Edit Personal Skills</h5>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-4">Skill</div>
              <div class="col-md-8">
                <div class="input-group">
                  <input type="hidden" id="skill_id" class="form-control">
                  <input type="text" placeholder="Skill" id="edit_skill_name" class="form-control" value="{{$personal_skill->skill}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-cogs"></i></span>
                  </div>
                </div>
              </div>  
            </div><br>
         
            <div class="row">
              <div class="col-md-4">Level</div>
              <div class="col-md-8">
                  <select class="form-control" style="width:100%" id="edit_skill_level">
                        <option value="">Select Level</option>
                        <option value='basic' @if($personal_skill->level=='basic') selected @endif>Basic</option>
                        <option value='intermediate' @if($personal_skill->level=='intermediate') selected @endif>Intermediate</option>
                        <option value='exepert' @if($personal_skill->level=='exepert') selected @endif>Exepert</option>
                  </select>
              </div>
            </div><br>
          </div>  
          <div class="modal-footer">
            <div class="left-side">
              <button type="button" id="updatePersonalSkills" class="btn btn-default btn-link" data-dismiss="modal">Update</button>
            </div>
            <div class="divider"></div>
            <div class="right-side">
              <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
    </div>
   </div>
   @endforeach
 @else
 @endif
 <div class="modal fade" id="addPersonalSkills" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h5 class="modal-title text-center" id="exampleModalLabel">Add Personal Skills</h5>
        </div>
        <div class="modal-body"> 
          <div class="row">
            <div class="col-md-4">Skill</div>
            <div class="col-md-8"> 
              <div class="input-group">
                <input type="hidden" id="skill_id" class="form-control" value="{{$personal_skill->id}}">
                <input type="text" placeholder="Skill" id="skill_name" class="form-control" >
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-cogs"></i></span>
                </div>
              </div>
            </div>
          </div><br>
       
          <div class="row">
            <div class="col-md-4">Level</div>
            <div class="col-md-8"> 
                <select class="form-control" style="width:100%" id="skill_level">
                      <option value="">Select Level</option>
                      <option value='basic'>Basic</option>
                      <option value='intermediate'>Intermediate</option>
                      <option value='exepert'>Exepert</option>
                </select>
            </div>
          </div><br>
        </div>
        <div class="modal-footer">
          <div class="left-side">
            <button type="button" id="savePersonalSkills" class="btn btn-default btn-link" data-dismiss="modal">Save</button>
          </div>
          <div class="divider"></div>
          <div class="right-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
  </div>
  </div>  
 @push('scripts')
 <script>
 var master_percentage_old = $('#master_percentage').val();
//  console.log(master_percentage_old);
  $('#master_percentage').change(function(){
    var master_percentage = $('#master_percentage').val();
    console.log(master_percentage);
    if(master_percentage>100){
      $("#warning").modal('show');
      $('#master_percentage').val(master_percentage_old);
    }
  });
  </script>
 @endpush