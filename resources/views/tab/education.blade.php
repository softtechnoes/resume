<div class="tab-pane text-center" id="education" role="tabpanel">
  <h3 class="text-muted">Education</h3>
  <!-- Section Education -->
  <div class="section experience scrollspy" id="experience-section">
    <!-- Title -->
    <div class="row">
        <div class="col s12 m12 l12">
            {{-- <h5 class="grey-text text-darken-2 left-align">Education</h5> --}}
        </div>
    </div>
    <!-- Education Items Items -->  
    <div class="row">
      <div class="col-md-6">
        <div class="card" data-color="orange" data-background="color">
          <div class="card-body text-center">
            <h3 class="card-category">
              {{-- <i class="fa fa-dribbble" aria-hidden="true"></i> --}}
              <i class='fa fa-book'></i> High School
            </h3>
            <h5 class="card-title">
              <a href="#pablo">"Good Design Is as Little Design as Possible"</a>
            </h5>
            @if($high_school!=null)
              <p class="card-description">
                  <table class="table table-hover info_table">
                    <tr>
                      <td>School Name</td>
                      <td>
                        <span id="school_updated"></span>
                        <span id="school_fresh">{{ $high_school->school_name }}</span></td>
                    </tr>
                    <tr>
                        <td>Board</td>
                        <td>
                          <span id="board_updated"></span>
                          <span id='board_fresh' board_id="{{$high_school->board->id}}">{{$high_school->board->board_name}}</span></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td>
                          <span id="state_updated"></span>
                          <span id="state_fresh" state_id="{{$high_school->states->id}}">{{$high_school->states->name}}</span></td>
                    </tr>
                    <tr>
                      <td>Percentage</td>
                      <td>
                        <span id="percentage_updated"></span>
                        <span id="percentage_fresh">{{ $high_school->percentage }}</span></td>
                    </tr>
                    <tr>
                      <td>Passing Year</td>
                      <td>
                        <span id="passing_year_updated"></span>
                        <span id="passing_year_fresh">{{ $high_school->passing_year }}</span></td>
                    </tr>
                    <tr>
                      <td>School Address</td>
                      <td>
                        <span id="school_address_updated"></span>
                        <span id="school_address_fresh">{{ $high_school->school_address }}</span></td>
                    </tr>
                  </table>
              </p>
              <div class="card-footer text-center">
                <button type="button" class="btn btn-round btn-success" id="editHighSchoolbtn" data-toggle="modal" data-target="#editHighSchool">
                    <i class="fa fa-edit"></i> Edit</button> 
              </div>
            @else 
              <div class="card-footer text-center">
                <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#addHighSchool">
                    <i class="fa fa-plus"></i> Add High School</button> 
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card" data-color="purple" data-background="color">
          <div class="card-body text-center">
            <h3 class="card-category">
              {{-- <i class="fa fa-dribbble" aria-hidden="true"></i> --}}
              <i class='fa fa-book'></i> Intermediate
            </h3>
            <h5 class="card-title">
              <a href="#pablo">"Good Design Is as Little Design as Possible"</a>
            </h5>
            @if($intermediate!=null)
              <p class="card-description">
                  <table class="table table-hover info_table">
                    <tr>
                      <td>School Name</td>
                      <td><span id="inter_school_updated"></span><span id="inter_school_fresh">{{ $intermediate->school_name }}</span></td>
                    </tr>
                    <tr>
                        <td>Board</td>
                        <td><span id="inter_board_updated"></span><span id='board_fresh'>{{ $intermediate->board->board_name }}</span></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><span id="inter_state_updated"></span><span id="inter_state_fresh">{{ $intermediate->states->name }}</span></td>
                    </tr>
                    <tr>
                      
                      <td>Percentage</td>
                      <td><span id="inter_percentage_updated"></span><span id="inter_percentage_fresh">{{ $intermediate->percentage }}</span></td>
                    </tr>
                    <tr>
                      <td>Passing Year</td>
                      <td><span id="inter_passing_year_updated"></span><span id="inter_passing_year_fresh">{{ $intermediate->passing_year }}</span></td>
                    </tr>
                    <tr>
                      <td>School Address</td>
                      <td><span id="inter_school_address_updated"></span><span id="inter_school_address_fresh">{{ $intermediate->school_address }}</span></td>
                    </tr>
                  </table>
              </p>
              <div class="card-footer text-center">
                <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#editIntermediate">
                    <i class="fa fa-edit"></i> Edit</button> 
              </div>
            @else
              <div class="card-footer text-center">
                <button type="button" class="btn btn-round btn-info" data-toggle="modal" data-target="#addIntermediate"><i class="fa fa-plus"></i> Add Intermediate</button>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>

    <!-- College Items Starts-->
    <div class="row">
        <!-- Graduation Starts-->
        <div class="col-md-6">
          <div class="card" data-color="purple" data-background="color">
            <div class="card-body text-center">
              <h3 class="card-category">
                {{-- <i class="fa fa-dribbble" aria-hidden="true"></i> --}}
                <i class='fa fa-graduation-cap'></i> Graduation
              </h3>
              <h5 class="card-title">
                <a href="#pablo">"Good Design Is as Little Design as Possible"</a>
              </h5>
              @if($graduation!=null)
              <p class="card-description">
                  <table class="table table-hover info_table">
                    <tr>
                      <td>Course Name</td>
                      <td><span id="master_course_updated"></span><span id="master_course_fresh">{{ $graduation->course_name }}</span></td>
                    </tr>
                    <tr>
                      <td>College Name</td>
                      <td><span id="master_college_updated"></span><span id="master_college_fresh">{{ $graduation->college }}</span></td>
                    </tr>
                    <tr>
                        <td>University</td>
                        <td><span id="master_university_updated"></span><span id="master_university_fresh">{{ $graduation->university }}</span></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><span id="master_state_updated"></span><span id="master_state_fresh">{{ $graduation->states->name }}</span></td>
                    </tr>
                    <tr>
                      <td>Percentage</td>
                      <td><span id="master_percentage_updated"></span><span id="master_percentage_fresh">{{ $graduation->percentage }}%</span></td>
                    </tr>
                    <tr>
                      <td>Passing Year</td>
                      <td><span id="master_passing_year_updated"></span><span id="master_passing_year_fresh">{{ $graduation->passing_year }}</span></td>
                    </tr>
                    <tr>
                      <td>From</td>
                      <td><span id="master_from_updated"></span><span id="master_from_fresh">{{ $graduation->from }}</span></td>
                    </tr>
                    <tr>
                      <td>To</td>
                      <td><span id="master_to_updated"></span><span id="master_to_fresh">{{ $graduation->to }}</span></td>
                    </tr>
                    <tr>
                      <td>College Address</td>
                      <td><span id="master_college_address_updated"></span><span id="master_college_address_fresh">{{ $graduation->college_address }}</span></td>
                    </tr>
                    <tr>
                      <td>Specialization</td>
                      <td><span id="master_specialization_updated"></span><span id="master_specialization_fresh">{{ $graduation->specialization }}</span></td>
                    </tr>
                  </table>
              </p>
              <div class="card-footer text-center">
                {{-- <a href="#pablo" rel="tooltip" title="Bookmark" class="btn btn-outline-neutral btn-round btn-just-icon"><i class="fa fa-bookmark-o"></i></a> --}}
                {{-- <a href="#pablo" class="btn btn-neutral btn-round"><i class="fa fa-edit"></i> Edit</a> --}}
                <button type="button" class="btn btn-round btn-info" data-toggle="modal" data-target="#editGraduation">
                    <i class="fa fa-edit"></i> Edit</button> 
              </div>
              @else
                <div class="card-footer text-center">
                  <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#addGraduation"><i class="fa fa-plus"></i>Add Graduation</button>
                </div>
              @endif
            </div>
          </div>
        </div>
        <!-- Graduation Ends-->
        <!-- Masters Starts-->
        <div class="col-md-6">
          <div class="card" data-color="orange" data-background="color">
            <div class="card-body text-center">
              <h3 class="card-category">
                {{-- <i class="fa fa-dribbble" aria-hidden="true"></i> --}}
                <i class='fa fa-graduation-cap'></i> Master
              </h3>
              <h5 class="card-title">
                <a href="#pablo">"Good Design Is as Little Design as Possible"</a>
              </h5>
              @if($master!=null)
              <p class="card-description">
                  <table class="table table-hover info_table">
                    <tr>
                      <td>Course Name</td>
                      <td><span id="master_course_updated"></span><span id="master_course_fresh">{{ $master->course_name }}</span></td>
                    </tr>
                    <tr>
                      <td>College Name</td>
                      <td><span id="master_college_updated"></span><span id="master_college_fresh">{{ $master->college }}</span></td>
                    </tr>
                    <tr>
                        <td>University</td>
                        <td><span id="master_university_updated"></span><span id="master_university_fresh">{{ $master->university }}</span></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><span id="master_state_updated"></span><span id="master_state_fresh">{{ $master->states->name }}</span></td>
                    </tr>
                    <tr>
                      <td>Percentage</td>
                      <td><span id="master_percentage_updated"></span><span id="master_percentage_fresh">{{ $master->percentage }}</span></td>
                    </tr>
                    <tr>
                      <td>Passing Year</td>
                      <td><span id="master_passing_year_updated"></span><span id="master_passing_year_fresh">{{ $master->passing_year }}</span></td>
                    </tr>
                    <tr>
                      <td>From</td>
                      <td><span id="master_from_updated"></span><span id="master_from_fresh">{{ $master->from }}</span></td>
                    </tr>
                    <tr>
                      <td>To</td>
                      <td><span id="master_to_updated"></span><span id="master_to_fresh">{{ $master->to }}</span></td>
                    </tr>
                    <tr>
                      <td>College Address</td>
                      <td><span id="master_college_address_updated"></span><span id="master_college_address_fresh">{{ $master->college_address }}</span></td>
                    </tr>
                    <tr>
                      <td>Specialization</td>
                      <td><span id="master_specialization_updated"></span><span id="master_specialization_fresh">{{ $master->specialization }}</span></td>
                    </tr>
                  </table>
              </p>
              <div class="card-footer text-center">
                <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#editMaster">
                    <i class="fa fa-edit"></i> Edit</button> 
              </div>
              @else
                <div class="card-footer text-center">
                  <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#addMaster"><i class="fa fa-plus"></i>Add Master</button>
                </div>
              @endif
            </div>
          </div>
        </div>
        <!-- Graduation Ends-->
    </div>
    <div class="row">
      <!-- Diploma Starts-->
        <div class="col-md-6">
          <div class="card" data-color="orange" data-background="color">
            <div class="card-body text-center">
              <h3 class="card-category">
                {{-- <i class="fa fa-dribbble" aria-hidden="true"></i> --}}
                <i class='fa fa-graduation-cap'></i> Diploma
              </h3>
              <h5 class="card-title">
                <a href="#pablo">"Good Design Is as Little Design as Possible"</a>
              </h5>
              @if($diploma!=null)
                <p class="card-description">
                  <table class="table table-hover info_table">
                    <tr>
                      <td>Course Name</td>
                      <td><span id="diploma_course_updated"></span><span id="diploma_course_fresh">{{ $diploma->course_name }}</span></td>
                    </tr>
                    <tr>
                      <td>College Name</td>
                      <td><span id="diploma_college_updated"></span><span id="diploma_college_fresh">{{ $diploma->college }}</span></td>
                    </tr>
                    <tr>
                        <td>University</td>
                        <td><span id="diploma_university_updated"></span><span id="diploma_university_fresh">{{ $diploma->university }}</span></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><span id="diploma_state_updated"></span><span id="diploma_state_fresh">{{ $diploma->states->name }}</span></td>
                    </tr>
                    <tr>
                      <td>Percentage</td>
                      <td><span id="diploma_percentage_updated"></span><span id="diploma_percentage_fresh">{{ $diploma->percentage }}%</span></td>
                    </tr>
                    <tr>
                      <td>Passing Year</td>
                      <td><span id="diploma_passing_year_updated"></span><span id="diploma_passing_year_fresh">{{ $diploma->passing_year }}</span></td>
                    </tr>
                    <tr>
                      <td>From</td>
                      <td><span id="diploma_from_updated"></span><span id="diploma_from_fresh">{{ $diploma->from }}</span></td>
                    </tr>
                    <tr>
                      <td>To</td>
                      <td><span id="diploma_to_updated"></span><span id="diploma_to_fresh">{{ $diploma->to }}</span></td>
                    </tr>
                    <tr>
                      <td>College Address</td>
                      <td><span id="diploma_college_address_updated"></span><span id="diploma_college_address_fresh">{{ $diploma->college_address }}</span></td>
                    </tr>
                    <tr>
                      <td>Specialization</td>
                      <td><span id="diploma_specialization_updated"></span><span id="diploma_specialization_fresh">{{ $diploma->specialization }}</span></td>
                    </tr>
                  </table>
                </p>
                <div class="card-footer text-center">
                  <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#editDiploma"><i class="fa fa-edit"></i> Edit Diploma</button>
                </div>
              @else
                <div class="card-footer text-center">
                  <button type="button" class="btn btn-round btn-success" data-toggle="modal" data-target="#addDiploma"><i class="fa fa-plus"></i>Add Diploma</button>
                </div>
              @endif
            </div>
          </div>
        </div>
      <!-- Diploma Ends-->
    </div>
    <!-- College Items Ends-->
  </div>
</div>