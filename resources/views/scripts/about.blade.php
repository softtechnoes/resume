<script>
    $("#editModal").click(function(){
        var about = $('#my_about').text();
        $('#user_about').html(about);
    });

    $("#updateAbout").click(function(){
        var about = $('#user_about').val();
        $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        $.ajax({
            type:"GET",
            url:"{{url('update-about')}}",
            data:{
            'about':about,
            }, 
            success:function(data){ 
            $("#my_about").html(data['about']);
            $.growl.notice({ message: "About Updated !" });
            }
        });
    });

    
</script>