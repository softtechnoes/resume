<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queries extends Model
{
    use Uuids;
    protected $connection='mysql';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table        = 'queries';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // Primary key
    public $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable     = ['id','name','email','message','ip'];

    // Relations
}
