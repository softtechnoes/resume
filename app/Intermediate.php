<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intermediate extends Model
{
    protected $table        = 'intermediate';
    protected $fillable     = ['id','user_id','board_name','state','passing_year','percentage','school_name','school_address'];
    
    /**
     * Get the related board.
     */
    public function board()
    {
        return $this->hasOne('App\EducationBoards','id', 'board_id');
    }
    /**
     * Get the related state.
     */
    public function states()
    {
        return $this->hasOne('App\State', 'id','state_id');
    }
}
