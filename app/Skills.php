<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Uuids as Uuids;

class Skills extends Model
{
	use Uuids;
    
    //use SoftDeletes;

    protected $connection='mysql';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table        = 'user_skills';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // Primary key
    public $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable     = ['id','user_id','skill','level','skill_type'];

    // Relations


}
