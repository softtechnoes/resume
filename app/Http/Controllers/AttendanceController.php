<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Attendance;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
// OR
use Artesaos\SEOTools\Facades\SEOTools;

class AttendanceController extends Controller
{
    public function index(){
        $data = Attendance::where('user_id',Auth::user()->id)->get();
        
        return view('attendance',compact('data'));

    }
    public function create(){
        $requestdata['user_id'] = Auth::user()->id;
        $addSkills = Attendance::create($requestdata);
        $data = 'success';
        return response()->json($data);
    }


    public function store(Request $request)
    {
        Task::create($request->all());
        return redirect()->route('tasks.index');
    }

    
    function index1() {
        SEOMeta::setTitle('Home');
        SEOMeta::setDescription('Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes.');
        SEOMeta::setCanonical('https://softtechnoes.xyz');

        OpenGraph::setDescription('Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes.');
        OpenGraph::setTitle('Home');
        OpenGraph::setUrl('https://softtechnoes.xyz');
        OpenGraph::addProperty('type', 'articles');

        TwitterCard::setTitle('Soft Technoes');
        TwitterCard::setSite('@softtechnoes');

        JsonLd::setTitle('Soft Technoes');
        JsonLd::setDescription('Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes.');
        JsonLd::addImage('https://www.softtechnoes.xyz/public/soft_technoes.jpg');
        
        return view('index');
    }
}
