<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $code = rand(100000,999999); 
        // $user_name = array('name'=>$data['name'],'code'=>$code,'token'=>$data['_token']);
        // Mail::send('auth.mail',$user_name, 
        // function($message) use ($data) {
        //     $message->to($data['email'], $data['name'])->subject
        //     ('Email Confirmation');
        //     $message->from('Admin@soft.com','Soft Technoes');
        // });
        $MAC = exec('getmac'); 
        $MAC = strtok($MAC, ' '); 
        
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'mac'=>$MAC,
            'ip' => \Request::ip(),
            'token' =>$data['_token'],
            'code' => $code,
        ]);
    }
    public function verifyUser($token)
    {
        // dd('test');
        $verifyUser = User::where('token', $token)->first();
        //dd($verifyUser);
        if(isset($verifyUser) ){
            $email = $verifyUser->email;
            //dd($verifyUser);
            if(!$verifyUser['verified']) {
                $verifyUser->verified = 1;
                $verifyUser->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/login')->with('status', $status);
    }
}
