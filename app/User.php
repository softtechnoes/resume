<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends \TCG\Voyager\Models\User
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mac','ip','is_delete','is_admin','verified','facebook_url','twitter_url','linkedin_url','instagram_url','image','about','age','citizenship','mac','ip','code','job','awards_won','address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
    * Get the related Skills
    */
    public function skills()
    {
        return $this->hasMany('App\Skills','user_id');
    }

    /**
    * Get the related Skills
    */
    public function high_school()
    {
        return $this->hasMany('App\Highschool','user_id');
    }

    /**
    * Get the related Skills
    */
    public function experience()
    {
        return $this->hasMany('App\Experience','user_id');
    }
}
