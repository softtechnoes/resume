<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
      
    use Uuids;
   
    protected $connection='mysql';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // Primary key
    public $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false; 

    protected $fillable = ['note','address','user_id','done','lat','lng'];

    // Relations
    /**
    * Get the related user owner
    */
    public function owner()
    {
        return $this->belongsTo('App\Models\Users\User','user_id');
    }
  
}
